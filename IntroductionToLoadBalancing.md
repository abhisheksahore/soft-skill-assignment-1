# **Introduction to Load Balancing**

Load balancing is a technique that requires a device called Load Balancer having public IP that helps us to divide the network traffic among different servers having private IP addresses. Load Balancers are used to improve the performance of the network they are used in. They enhance the capacity, flexibility, Availability of the network. They help the network to scale up and handles more and more traffic efficiently.
There are physical load balancers and virtual load balancers. 

**Physical Load Balancers** look similar to the routers and connected to them in the same way.   

**Virtual Load Balancer** is the program that emulates the presence of the hardware load balancer and works similar to the virtual servers.




## **How it works?** 
Load Balancers use methods like **Round Robin** and **Load based** methods.

**Round Robin** distributes the traffic alternatively to the available servers.

**Load Based** distribution works on two mechanisms. First is ***Least Connections***, in this Load Balancer turns the traffic to the server which is having less traffic or which is less occupied. The second one is ***Resource-Based***, in this Load balancer monitors the performance criteria of the servers like CPU Usage, Memory Consumption, etc., and based on that load balancer decides where to divert the traffic.


## **Benefits of using Load Balancer**

- Scalability
- Traffic Management
- Connectivity and Availability
- Flexibility


## **Scaling**

![](./vh.png)

### Vertical Scaling
**Vertical scaling** is scaling up the server by upgrading the server with high-performance hardware.

### Cost Efficiency of Vertical Scaling
![](./vs.png)

### Horizontal Scaling
**Horizontal Scaling** is scaling out by increasing the number of servers with the same configuration and hosting the same application. It is very ***cost-efficient*** as compared to vertical scaling. 


### Cost Efficiency of Horizontal Scaling
![](./hs.png)


### **References:**

[What Is Load Balancing? How Load Balancers Work - NGINX](https://www.radware.com/glossary/loadbalancing/ "What Is Load Balancing? How Load Balancers Work - NGINX")

[System Design: What is Horizontal vs Vertical Scaling?](https://www.youtube.com/watch?v=p1YQU5sEz4g "System Design: What is Horizontal vs Vertical Scaling?")

[System Design: What is Load Balancing?](https://www.youtube.com/watch?v=gMIslJN44P0 'System Design: What is Load Balancing?')

[Scaling Horizontally vs. Scaling Vertically](https://www.section.io/blog/scaling-horizontally-vs-vertically/ 'Scaling Horizontally vs. Scaling Vertically')
